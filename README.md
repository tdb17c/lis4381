> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
> Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.

# LIS4381 Mobile Web Application Development

## Trystan Bannon

### LIS4381 Requirements:

_Course Work Links:_

[A1 README.md](a1/README.md "My a1 README file")

- Install Git
- Create Bitbucket account, make repo
- Connect local to Bitbucket repository
- Install AMPPS
- Install Java
- Install Android Studio

[A2 ReadMe.md](a2/README.md "My a2 README file")

- Create Recipe app with two different screens
- Change the background and text color of the app
- Complete first 3 SkillSets (EvenorOdd, LargerNumber, Loops)

[A3 ReadMe.md](a3/README.md "My a3 README file")

- Create database on mySQL using given business rules
- Run database on a local host using AMMPS
- Create MyEvent app that calculates total price of different concerts
- Make a launcher icon for the app and add some visual attributes
- Complete 4-6 SkillSets (decisionStructures, RandomArrays, Methods)

[A4 ReadMe.md](a4/README.md "My a4 README file")

- Create local webpage to display all assignments
- Transfer all previous assinments screenshot to the webpage in there own tab
- Create working carosel with images, text, and links to outside web pages
- Create working client-side validation for a Petstore form
- Complete 10-12 SkillSets (ArrayList, NestedSttructures, TempatureConversion)

[A5 ReadMe.md](a5/README.md "My a5 README file")

- Build of of form made in assignment 4 and add it to a database
- Make sure both client side and server side validations are working appropriatly
- Present database in a table with appropriate colums
- Complete 13-15 SkillSets (Sphere Volume Calculator, Simple Calculator, Read/Write files)

[P1 ReadMe.md](p1/README.md "My P1 README file")

- Backwords engineer Buisness Card App from provided example
- Supply with additional features like borders and launcher icon
- Complete 7-9 SkiillSets (Random Array, Largest of 3, Array Runtime)

[P2 ReadMe.md](p2/README.md "My P2 README file")

- TBD
