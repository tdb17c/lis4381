> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
> Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.

# LIS4381 Mobile Web Application Development

## Trystan Bannon

### Assignment 4 Requirements:

_Requirements:_

1. Format php files to create personal website
2. Transfer all previous assignments to website on there own page
3. Create working client-side validation for the pet store form
4. Add text, images, and links to the home page carosel
5. Complete Skill Sets 10 - 12

#### README.md file should include the following items:

- Screenshot of incorrect and correct inputs in the petstore form
- Screenshot of carosel
- Screenshots of Skill sets

#### Assignment Screenshots:

_Screenshot of Carosel_:

![Carosel Screenshot](img/a4_carosel.png)

|                       Correct Inputs                       |                          Incorrect Inputs                           |
| :--------------------------------------------------------: | :-----------------------------------------------------------------: |
| ![Correct Inputs](img/a4_correct.png){height=100 width=50} | ![second user interface](img/a4_incorrect.png){height=100 width=50} |

|        SkillSet 10         |          SkillSet 11 pt.1           |          SkillSet 11 pt.2           |
| :------------------------: | :---------------------------------: | :---------------------------------: |
| ![ArrayList](img/SS10.png) | ![NestedStructures](img/SS11_1.png) | ![NestedStructures](img/SS11_2.png) |

|              SkillSet 12              |     SkillSet 12 (Bounus)      |
| :-----------------------------------: | :---------------------------: |
| ![Tempature Conversion](img/SS12.png) | ![PersonCLass](img/SS12E.png) |
