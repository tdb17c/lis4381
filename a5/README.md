# LIS4381 Mobile Web Application Development

## Trystan Bannon

### Assignment 5 Requirements:

1. Create database page to show database

- Put into correct colums and allow user to alter its arangement

2. Create forum from A4 to add to the database

- Make sure both client side and server side validations are working

3. Complete SKillSets

#### Assignment Screenshots:

_Screenshot of Working Database_:

![AMPPS Installation Screenshot](img/Capture.JPG)

_Screenshot of invalid user entered data and error message_:

![JDK Installation Screenshot](img/Capture2.JPG)

|      SkillSet 14       |    SkillSet 14 pt.2    |
| :--------------------: | :--------------------: |
| ![pt.1](img/SS141.JPG) | ![pt.2](img/SS142.JPG) |

|    SkillSet 14 pt.3    |    SkillSet 14 pt.4    |
| :--------------------: | :--------------------: |
| ![pt.3](img/SS143.JPG) | ![pt.4](img/SS144.JPG) |

|    SkillSet 15 pt.1    |   SkillSet 15 pt.2    |
| :--------------------: | :-------------------: |
| ![pt.1](img/SS151.JPG) | ![pt.2](img/SS15.JPG) |
