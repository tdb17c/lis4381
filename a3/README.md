# LIS4381 Mobile Web Application Development

## Trystan Bannon

### Assignment 3 Requirements:

_Parts of the Assignment:_

1. Pet store database
2. MyEvent App
3. SkillSets 4-6

#### Assignment Screenshots:

_Screenshot of Pet Store Database_:

![Petstore Database](img/a3database.png)

|                          First Page                          |                          Second Page                          |
| :----------------------------------------------------------: | :-----------------------------------------------------------: |
| ![first user interface](img/a3app1.png){height=100 width=50} | ![second user interface](img/a3app2.png){height=100 width=50} |

|             SkillSet 1             |          SkillSet 2          |       SkillSet 3        |
| :--------------------------------: | :--------------------------: | :---------------------: |
| ![DecisionStructures](img/SS4.png) | ![RandomArrays](img/SS5.png) | ![Methods](img/SS6.png) |
