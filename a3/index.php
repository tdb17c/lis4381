<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Mark K. Jowett, Ph.D.">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 3</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> A core feature to an application is the database it uses to gain and store new data to keep it interactive. In this assignment I formated a relatioanal database based off of a set of buisness rules. With the use of MySQL I connected different attributes and used forign keys to keep things connected with each other. Along side this I made an app that takes in user data and give out an output with that. The example here is asking
					the user to give a certian amout of tickets they want and pick what band they want to see, and the app tells them the total cost of the tickets.
				</p>
				<p class="text-justify"><strong>Solution:</strong></p>
				<ul>
					<li class="text-justify"><a href="docs/mwb">MWB</a></li>
					<li class="text-justify"><a href="docs/sql">SQL</a></li>
				</ul>
				

				<h4>Petstore Database</h4>
				<img src="img/a3database.png" class="img-responsive center-block" alt="JDK Installation">

				<h4>Main Page</h4>
				<img src="img/a3app1.png" class="img-responsive center-block" alt="Android Studio Installation" height="500" width="400">

				<h4>Page after Calculating Price</h4>
				<img src="img/a3app2.png" class="img-responsive center-block" alt="AMPPS Installation" height="500" width="400">
				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
