# LIS4381 Mobile Web Application Development

## Trystan Bannon

### Assignment 3 Requirements:

_Parts of the Assignment:_

1. Set up my Buisness card App
2. Provide more features to app
3. SkillSets 7-9

#### Assignment Screenshots:

|                        First Page                        |                        Second Page                        |
| :------------------------------------------------------: | :-------------------------------------------------------: |
| ![first user interface](img/a1.png){height=100 width=50} | ![second user interface](img/a2.png){height=100 width=50} |

|             SkillSet 1             |          SkillSet 2          |       SkillSet 3        |
| :--------------------------------: | :--------------------------: | :---------------------: |
| ![DecisionStructures](img/ss7.png) | ![RandomArrays](img/ss8.png) | ![Methods](img/ss9.png) |
