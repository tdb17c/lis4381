<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Trystan D. Bannon">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> This project was a coelation of all the previous assignments before it, using all those skills to make a online buisness card showing of my information and intrest. It involved making two different pages, one with my profile picture and the other a general description and contact info on me.
				</p>
				<p class="text-justify"><strong>Requirements:</strong> Backwords engineer provided screeenshots of final product and include a icon image.</p>

				<h4>Main Page</h4>
				<img src="img/a1.png" class="img-responsive center-block" alt="Main Page" height="500" width="400">

				<h4>Second Page</h4>
				<img src="img/a2.png" class="img-responsive center-block" alt="Second Page" height="500" width="400">

				
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
