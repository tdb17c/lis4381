<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Trystan D. Bannon">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<strong>Description:</strong> This assignment had me explore more of the applications with Android Studio Code and create an simple interactive app. This app had a main page that showed off the final product and when clicking the button it brought me to a new page. This page showed the recipe but kept all the structure of the main page.
				</p>

				<h4>Main Page</h4>
				<img src="img/front.png" class="img-responsive center-block" alt="Main Page" height="500" width="400">

				<h4>Recipe Page</h4>
				<img src="img/second.png" class="img-responsive center-block" alt="Recipe Page" height="500" width="400">

				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
