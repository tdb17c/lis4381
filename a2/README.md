> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
> Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.

# LIS4381 Mobile Web Application Development

## Trystan Bannon

### Assignment 2 Requirements:

_Two Parts:_

1. Create working app that displays a Bruschetta Recipe
2. Understand some basic Java coding concepts

#### README.md file should include the following items:

- Screenshot of both pages of my application
- Screenshot of all three skill sets

#### Assignment Screenshots:

|                    First Page                     |                     Second Page                     |
| :-----------------------------------------------: | :-------------------------------------------------: |
| ![Front page](img/front.png){height=100 width=50} | ![second page](img/second.png){height=100 width=50} |

|           SkillSet 1            |              SkillSet 2               |       SkillSet 3        |
| :-----------------------------: | :-----------------------------------: | :---------------------: |
| ![EvenorOdd](img/EvenorOdd.png) | ![LargerNumber](img/LargerNumber.png) | ![loops](img/loops.png) |
