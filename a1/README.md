> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
> Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.

# LIS4381 Mobile Web Application Development

## Trystan Bannon

### Assignment 1 Requirements:

_Three Parts:_

1. Distributed Version Comtrol with Git and Bitbucket
2. Development Installations
3. Chapter Questions (1,2)

#### README.md file should include the following items:

- Screenshot of AMPPS Installation
- Screenshot of running java Hello World
- Scrrenshot of running Android Studio - My First App
- Git commands with short discritions
- Bitbucket repo links

> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init: Initilizes current directory as a git reposotory
2. git status: list what files have been commited or not
3. git add: adds all changes to the next commit
4. git commit: stages the changes to get ready for a push to repo
5. git push: transfers commits and changes to bitbucket
6. git pull: gets intended info from bitbucket to your local repository
7. git log: shows all the commit made for that repository

#### Assignment Screenshots:

_Screenshot of AMPPS running http://localhost_:

![AMPPS Installation Screenshot](Images/ampps1.png)
![AMPPS Installation Screenshot](Images/ampps2.png)

_Screenshot of running java Hello_:

![JDK Installation Screenshot](Images/java.png)

_Screenshot of Android Studio - My First App_:

![Android Studio Installation Screenshot](Images/ASP.png)

#### Tutorial Links:

_Bitbucket Tutorial - Station Locations:_
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/tdb17c/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")
